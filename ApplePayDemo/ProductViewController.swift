//
//  ProductViewController.swift
//  ApplePayDemo
//
//  Created by Michael Mendoza on 2/6/17.
//  Copyright © 2017 Michael Mendoza. All rights reserved.
//

import UIKit

class ProductViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var priceLabel: UILabel!
    
    var product = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI(product)
    }
    
    fileprivate func setupUI(_ product: String) {
        title = product
        titleLabel.text = "Pay for your new \(product)."
        
        if product == "Mapple" {
            productImageView.image = UIImage(named: "MacBookPurchase")
            priceLabel.text = "$12,999.00"
        } else if product == "Mapple Air" {
            productImageView.image = UIImage(named: "MacBookAirPurchase")
            priceLabel.text = "$9,999.00"
        } else if product == "Mapple Pro" {
            productImageView.image = UIImage(named: "MacBookProPurchase")
            priceLabel.text = "$23,999.00"
        }
    }
    
}
