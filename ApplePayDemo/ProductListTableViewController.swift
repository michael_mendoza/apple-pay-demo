//
//  ProductListTableViewController.swift
//  ApplePayDemo
//
//  Created by Michael Mendoza on 2/6/17.
//  Copyright © 2017 Michael Mendoza. All rights reserved.
//

import UIKit

class ProductListTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let viewController = segue.destination as! ProductViewController
        
        if segue.identifier == "Mapple" {
            print("Selected Mapple")
            viewController.product = "Mapple"
            
        } else if segue.identifier == "MappleAir" {
            print("Selected Mapple Air")
            viewController.product = "Mapple Air"
            
        } else if segue.identifier == "MapplePro" {
            print("Selected Mapple Pro")
            viewController.product = "Mapple Pro"
        }
    }
}
